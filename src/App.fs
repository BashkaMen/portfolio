module App.View

open Elmish
open Portfolio
open Fable.Core
open Fable.Core.JsInterop

importAll "../sass/main.sass"

open Fable.React
open Fable.React.Props



open Elmish.React
open Elmish.Debug
open Elmish.HMR

// App
Program.mkSimple Main.init Main.update Main.view
#if DEBUG
|> Program.withDebugger
#endif
|> Program.withReactBatched "elmish-app"
|> Program.run
