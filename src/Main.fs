﻿module Portfolio.Main

open Fable.React
open Fable.React.Props
open System


type Side =
    | Left
    | Right

type Contact =
    | Email of string
    | Phone of string
    | Telegram of string
    | YouTube of string
    | GitHub of string
    | GitLab of string


type WorkPlaceEndDate =
    | Now
    | Date of DateTime
    | Future

type WorkPlace =
    { Title: string
      Position: string
      Image: string
      From: DateTime
      To: WorkPlaceEndDate
      Description: string }


type Skill = Skill of title:string

type State =
    { Avatar: string
      FullName: string
      Description: string
      Position: string
      Contacts: Contact list
      SKills: Skill list
      WorkPlaces: WorkPlace list
      NeedWork: bool }



let init () =
    { Avatar = "/img/avatar.jpg"
      FullName = "Dmitriy Bashinskiy"
      Description = "I am middle .NET developer. I can to develop Frontend, Backend, Desktop, Mobile apps. A dedicated team player."
      Position = "Strong Middle .NET Developer"
      Contacts =
          [ Email "sbmbash@gmail.com"
            Telegram "@BashkaMen"
            YouTube "https://www.youtube.com/c/BashkaMen"
            Phone "+38 (098) 430-38-29"
            GitHub "https://github.com/BashkaMen"
            GitLab "https://gitlab.com/BashkaMen"]
      SKills = [
            Skill "C# 5 years";
            Skill "F# 0.5 years"
            Skill "SQL (EF, NHibernate, Dapper)"
            Skill "NoSql (MongoDB, CosmosDB)"
            Skill "Redis (cache, pub\sub, queue)"
            Skill "RabbitMq"
            Skill "ASP NET MVC\WebAPI"
            Skill "GRPC"
            Skill "Docker (simple usage and swarm)"
            Skill "Microservices"
            Skill "SAGAs"
            Skill "CQRS"
            Skill "Event Sourcing (only home mage)"
            Skill "DDD"
            Skill "Design patterns"
            Skill "VueJs"
            Skill "Angular"
            Skill "ReactJs with Fable"
            Skill "WPF\UWP + MVVM"
            Skill "Xamarin"
            Skill "Git flow"
            Skill "Azure services"
            Skill "Cloud"
            Skill "Agile"
            Skill "Selenium\Selenoid"
            Skill "Communicability"
            Skill "Flexibility"
            Skill "Sense of humor"
        ]
      WorkPlaces = [
          {
              Title = "MoneyVeo"
              Position = "Backend Developer"
              Image = "/img/moneyveo.jpg"
              Description = "Разработка системы для автоматической выдачи микрозаймов. В основном, разрабатываю бекенд, также участвую в разработке фронта (razor) и админки. Перевожу монолит на микросервисную архитектуру."
              From = DateTime(2019, 11, 28)
              To = Now
          }
          {
              Title = "TAS Insurance"
              Position = "Backend Developer"
              Image = "/img/tas.png"
              Description = "Разработка сайта для продажи страховок. На сайте вводят всю нужную информацию о клиенте, после чего заявление обрабатывается в несколько этапов."
              From = DateTime(2019, 4, 28)
              To = Date (DateTime(2019, 11, 17))
          }
          {
              Title = "Wirex"
              Position = "Backend Developer"
              Image = "/img/wirex.webp"
              Description = "Разработка платежной системы с криптовалютой и фиатными средствами. Разработка фреймворка для тестирования. Покрытие E2E тестами как Backend, так и ThirdParty"
              From = DateTime(2018, 12, 5)
              To = Date (DateTime(2019, 4, 15))
          }
          {
              Title = "Alfa-Bank"
              Position = "Full Stack Web Develop"
              Image = "/img/alfa-bank.png"
              Description = "Занимаюсь разработкой в проектах: “AlfaCollection” - CRM-система для работы с должниками, “Чат-Банк” - бот для денежных переводов в месенджерах. Вел курсы по основам C# и автотестам для тестировщиков нашего отдела. Участвовал в различных хакатонах."
              From = DateTime(2018, 01, 5)
              To = Date (DateTime(2018, 11, 26))
          }
          {
              Title = "Dream Project"
              Image = "/img/dream-develop.png"
              Description = "Хочу и пытаюсь реализовать интересный проект от себя, который будет востребован и ценен как для меня, так и пользователям. Возможно, когда-то получится и о нем узнает мир!"
              Position = "Full Stack Developer"
              From = DateTime(2015, 7, 1)
              To = Now
          }
          {
              Title = "Freelance"
              Image = "/img/freelance.png"
              Description = "Занимался разработкой различных решений на заказ. От простых парсеров до автоматизированных систем для увеличения продуктивности работы."
              Position = "Full Stack Developer"
              From = DateTime(2015, 7, 1)
              To = Now
          }
      ]
      NeedWork = true }


let update msg state = state



let header =
    div [ ClassName "header" ] [
        div [ ClassName "logo" ] [ str "RESUME" ]
        div [ ClassName "menu-container" ] [
            div [ ClassName "menu-item" ] [ a [ ] [ str "Main" ] ]
            div [ ClassName "menu-item" ] [ a [ Href "#contacts" ] [ str "Contacts" ] ]
            div [ ClassName "menu-item" ] [ a [ Href "#skills" ] [ str "Skills" ] ]
            div [ ClassName "menu-item" ] [ a [ Href "#works" ] [ str "Work Places" ] ]
        ]

        div [ ClassName "head-buttons" ] [
            a [ ClassName "hire-btn"; Href "/pdf/resume.pdf"; Target "_blank" ] [ str "OPEN PDF" ]
            a [ ClassName "hire-btn"; Href "https://t.me/BashkaMen"; Target "_blank" ] [ str "HIRE ME!" ]
        ]
    ]


let personalBlock state =
    div [ ClassName "personal-info" ] [
        div [ ClassName "avatar-wave" ] [
            img [ Src state.Avatar
                  ClassName "avatar" ]
        ]

        div [ ClassName "details" ] [
            h2 [ ClassName "name" ] [
                str state.FullName
            ]
            h5 [ ClassName "position" ] [
                str state.Position
            ]
            hr []
            h5 [ ClassName "description" ] [
                str state.Description
            ]
        ]
    ]

let renderContact contact =
    let item icon element =
        div [ ClassName "contact-item" ] [
            i [ ClassName icon ] []
            element
        ]

    div [] [
        match contact with
        | YouTube url -> item "fab fa-youtube" <| a [ Href url; Target "_blank" ] [ str "BashkaMen" ]
        | Telegram x -> item "fab fa-telegram" <| str x
        | Email x -> item "fas fa-at" <| str x
        | Phone x -> item "fas fa-mobile" <| str x
        | GitHub x -> item "fab fa-github" <| a [ Href x; Target "_blank" ] [ str "Link" ]
        | GitLab x -> item "fab fa-gitlab" <| a [ Href x; Target "_blank" ] [ str "Link" ]
    ]

let contacts contacts =
    div [] [
        hr []
        contacts
        |> List.map renderContact
        |> div [ ClassName "contacts" ]
        hr []
    ]


let renderSkill (Skill skill) =
    div [ ClassName "skill-item" ] [ str skill ]

let skills skills =
    let column elements =
        div [ ClassName "column" ] [
            yield! elements |> List.map renderSkill
        ]

    let skillGroups =
        skills
        |> List.indexed
        |> List.groupBy (fun (i, x) -> i / 7)
        |> List.map (fun (_, list) -> List.map snd list)
        |> List.map (fun x -> column x)

    div [ ClassName "skills" ] [
        yield! skillGroups
    ]


let renderDate date =
    match date with
    | Now -> "Now"
    | Future -> "Future"
    | Date date -> date.ToString("dd.MM.yyyy")


let renderWorkPlace workPlace =
    div [ ClassName "work-place" ] [
        img [ ClassName "avatar"; Src workPlace.Image ]
        div [ ClassName "work-info" ] [
            span [ ClassName "title" ] [ str workPlace.Title ]

            let dates = sprintf " (%s - %s)" (workPlace.From.ToString("dd.MM.yyyy")) (renderDate workPlace.To)
            span [ ClassName "sub-title" ] [ str (sprintf "%s %s" workPlace.Position dates) ]
            div [ ClassName "description" ] [ str workPlace.Description ]
        ]
    ]

let workPlaces places needWork =
    let futureWorkPlace =
        { Title = "Your company name"
          Position = "Middle .NET Developer"
          Image =
              "/img/hire-me.png"
          From = DateTime.Now.AddDays(30.)
          To = Future
          Description = "I develop your project moving it to success!" }


    div [ ClassName "work-places-container" ] [
        yield! places
        |> fun x -> if needWork then futureWorkPlace :: x else x
        |> List.map renderWorkPlace
        |> List.collect (fun x -> [ hr[]; x ])
    ]


let view state dispatch =
    let anchor key = a [ Id key; ClassName "anchor" ] []
    div [] [
        header
        personalBlock state
        anchor "contacts"
        contacts state.Contacts
        anchor "skills"
        skills state.SKills
        anchor "works"
        workPlaces state.WorkPlaces state.NeedWork
        div [ Style [ Height 50 ] ] []
    ]
